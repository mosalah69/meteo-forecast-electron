import Forecast from './components/Forecast';
import React from 'react';

function App() {
  return (
    <div>
        <Forecast />
    </div>
  );
}

export default App;