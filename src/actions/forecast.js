export const updateForecast = (forecast) => {
    return {
        type: 'UPDATE_FORECAST',
        value: forecast
    }
};

export const toggleLoader = (status) => {
    return {
        type: 'TOGGLE_LOADER',
        value: status
    }
};

export const fetchForecast = (city) => {
    return async dispatch => {
        dispatch(toggleLoader(true));
        const response = await fetch('http://api.weatherstack.com/current?access_key=301421c617a1876e9903b7bf69698397&query=%27'+city);
        const data = await response.json();
        console.log(data);
        dispatch(updateForecast(data));
        dispatch(toggleLoader(false));
    }
};
